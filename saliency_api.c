/*
	filename: saliency_api.c
	Author: Gopalakrishna Hegde
	Date: 18 Dec 2014

*/
#include "saliency_api.h"
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "img_add.h"
#include "img_sub.h"
#include "normalize.h"
#include "img_resize.h"
#include "filter2D.h"

#ifdef DEBUG
#define LOG(ARGS...) printf(ARGS)
#else
#define LOG(ARGS...) 
#endif

// API to add two images. This does saturate add and devide by 2. Same as averaging two images

/*******kernel parameters**********/
int filter_kernel[5][9] = { {1,2,1,2,4,2,1,2,1},                // Gaussian kernel
                            {-6,15, -6, -7, 15,-7, -6, 15, -6}, // Gabor kernel 0 deg, fixed point 8 bit, 1Q4 format
                            {-14,2, 14,2, 15, 2, 14, 2,-14},    // Gabor kernel 45ded
                { -6, -7,-6,15,15,15,-6,-7,-6},     // Gabor kernel 90 deg
                {14,2, -14, 2, 15, 2, -14, 2, 14}   // Gabor kernel 135 deg
};

/****************/
void img_add(Mat *img_1, Mat *img_2, Mat *img_out)
{
	LOG("%s: Entry\n", __func__);
#ifdef SIZE_CHECK
	if ((img_1->no_rows != img_2->no_rows) || (img_1->no_cols != img_2->no_cols))
	{
		printf("%s:%d:%s Image size mismatch img1: %dx%d img2: %dx%d\n", __FILE__, __LINE__, __func__, img_1->no_rows,img_1->no_cols,img_2->no_rows,img_2->no_cols);
		return;
	}
#endif

	s_img_add_weighted(img_1->img_data, img_2->img_data, img_out->img_data ,img_1->no_rows, img_1->no_cols, 1);
	img_out->no_rows = img_1->no_rows;
	img_out->no_cols = img_1->no_cols;

	LOG("%s: Exit\n", __func__);
}

// API to normalize the image
void img_normalize(Mat *in_img, Mat *norm_img)
{
	LOG("%s: Entry\n", __func__);

	s_img_normalize(in_img->img_data, norm_img->img_data, in_img->no_rows, in_img->no_cols);
	norm_img->no_rows = in_img->no_rows;
	norm_img->no_cols = in_img->no_cols;

	LOG("%s: Exit\n", __func__);
}

// API to subsample image by factor of 2
void img_subsample(Mat *in_img, Mat *out_img)
{
	LOG("%s: Entry\n", __func__);
    s_img_subsample(in_img->img_data, out_img->img_data, in_img->no_rows, in_img->no_cols);

    out_img->no_rows = ceil(in_img->no_rows/2);
    out_img->no_cols = ceil(in_img->no_cols/2);
	
	LOG("%s: Exit\n", __func__);
}

// API to double the size of the image
void img_interpolate(Mat *in_img, Mat *out_img)
{
	LOG("%s: Entry\n", __func__);

	s_img_interpolate(in_img->img_data, out_img->img_data, in_img->no_rows, in_img->no_cols);	

	out_img->no_rows = 2*in_img->no_rows;
	out_img->no_cols = 2*in_img->no_cols;

	LOG("%s: Exit\n", __func__);
}

// API to subtract two images. This finds absolute difference between 2 images
void img_subtract(Mat *img_1, Mat *img_2, Mat *diff_img)
{
	LOG("%s: Entry\n", __func__);
#ifdef SIZE_CHECK
	if ((img_1->no_rows != img_2->no_rows) || (img_1->no_cols != img_2->no_cols))
    {
	printf("%s:%d:%s Image size mismatch img1: %dx%d img2: %dx%d\n", __FILE__, __LINE__, __func__, img_1->no_rows,img_1->no_cols,img_2->no_rows,img_2->no_cols);
        return;
    }
#endif

    s_img_sub(img_1->img_data, img_2->img_data, diff_img->img_data ,img_1->no_rows, img_1->no_cols);
    diff_img->no_rows = img_1->no_rows;
    diff_img->no_cols = img_1->no_cols;

	LOG("%s: Exit\n", __func__);
}

// API to do Gaussian filter on imput image using 3x3 Gaussian kernel
void gaussian_blur(Mat *in_img, Mat *out_img)
{
	LOG("%s: Entry\n", __func__);

	s_filter2D(in_img->img_data, filter_kernel[0], out_img->img_data, in_img->no_rows, in_img->no_cols);
	out_img->no_rows = in_img->no_rows;
    out_img->no_cols = in_img->no_cols;

	LOG("%s: Exit\n", __func__);
}

// API to do 2D filtering on image using 3x3 kernel of specified type
void filter2D(Mat *in_img, Mat *out_img, FILTER_TYPE filter_type)
{
	LOG("%s: Entry\n", __func__);

	s_filter2D(in_img->img_data, filter_kernel[filter_type], out_img->img_data, in_img->no_rows, in_img->no_cols);
	out_img->no_rows = in_img->no_rows;
    out_img->no_cols = in_img->no_cols;

	LOG("%s: Exit\n", __func__);
}

