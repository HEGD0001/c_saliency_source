LIBS  = 
CFLAGS = -Wall -O3 -mfpu=neon
CC = gcc
OBJ = img_add.o		\
	img_sub.o		\
	img_resize.o	\
	normalize.o		\
	filter2D.o		\
	saliency_api.o	\
	saliency_main.o	

saliency_app: $(OBJ)
	$(CC) $(LIBS) -o $@ $^ $(CFLAGS) -lm -lgcc -lc
%.o : %.c
	$(CC) -c $(CFLAGS) $< -o $@


clean:
	rm *.o
	rm saliency_app
#LIBS  =
#CFLAGS = -Wall

# Should be equivalent to your list of C files, if you don't build selectively
#SRC=$(wildcard *.c)

#test: $(SRC)
#	gcc -o $@ $^ $(CFLAGS) $(LIBS)
