 #include "img_add.h"


void s_img_add_weighted(uint8_t *image1, uint8_t *image2, uint8_t *res_image, int M, int N, int scale)
{
        int row, col;
        for (row = 0; row < M; row++)
        {
                for(col = 0; col < N; col++)
                {
                        *(res_image + N*row +col) = (*(image1 +N* row+ col) + *(image2+row*N+col)) >> scale;
                }
        }

}
