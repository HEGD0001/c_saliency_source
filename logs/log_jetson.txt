****************************************
Trial Number 1
****************************************

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 240x160
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
2688,
6623,
11692,
21276,
55,
36,
20,
421,
113,
437,
415,
606,
715,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 320x240
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
5207,
13449,
23825,
42955,
111,
69,
40,
831,
229,
881,
868,
1254,
1442,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 480x320
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
10379,
26936,
48127,
86434,
255,
148,
83,
1679,
425,
1735,
1669,
2500,
2887,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 640x480
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
21063,
54715,
98204,
175865,
500,
310,
165,
3406,
865,
3526,
3339,
5089,
5840,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 1080x720
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
55451,
144533,
258247,
462965,
1222,
746,
465,
8541,
2268,
9237,
8498,
13536,
15511,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 1920x1080
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
149768,
390473,
698420,
1251127,
3382,
2054,
1361,
22702,
6227,
24741,
22685,
36590,
41890,


------------------ Unit testing END--------------- 
****************************************
Trial Number 2
****************************************

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 240x160
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
2593,
6635,
11762,
21256,
56,
36,
21,
422,
114,
438,
416,
605,
712,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 320x240
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
5196,
13412,
23916,
43015,
110,
69,
49,
823,
231,
878,
833,
1256,
1433,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 480x320
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
10456,
26885,
48182,
86509,
247,
148,
84,
1679,
430,
1726,
1667,
2499,
2887,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 640x480
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
21034,
54647,
98158,
175731,
502,
305,
165,
3345,
864,
3524,
3342,
5150,
5839,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 1080x720
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
55501,
144457,
258353,
463041,
1227,
762,
452,
8469,
2258,
9302,
8467,
13472,
15578,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 1920x1080
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
149729,
390407,
699359,
1251932,
3398,
2069,
1359,
22704,
6219,
24737,
22682,
36444,
41893,


------------------ Unit testing END--------------- 
****************************************
Trial Number 3
****************************************

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 240x160
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
2560,
6644,
11696,
21167,
55,
36,
21,
414,
113,
443,
409,
611,
764,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 320x240
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
5193,
13446,
23916,
43025,
110,
69,
41,
831,
225,
883,
832,
1252,
1439,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 480x320
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
10380,
26960,
48133,
86467,
249,
148,
83,
1678,
475,
1734,
1670,
2504,
2890,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 640x480
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
20973,
54686,
98093,
175623,
515,
297,
165,
3359,
866,
3520,
3347,
5093,
5834,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 1080x720
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
55442,
144506,
258363,
463057,
1207,
761,
456,
8466,
2255,
9241,
8530,
13487,
15500,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 1920x1080
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
149810,
390358,
698953,
1251562,
3392,
2066,
1361,
22702,
6224,
24741,
22703,
36447,
41889,


------------------ Unit testing END--------------- 
****************************************
Trial Number 4
****************************************

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 240x160
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
2597,
6635,
11694,
21192,
56,
36,
20,
420,
113,
436,
414,
605,
767,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 320x240
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
5172,
13393,
23908,
42968,
111,
69,
40,
832,
228,
884,
830,
1252,
1440,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 480x320
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
10385,
26976,
48099,
86445,
247,
149,
82,
1678,
477,
1735,
1668,
2502,
2891,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 640x480
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
20979,
54752,
98245,
175858,
499,
306,
164,
3349,
863,
3527,
3339,
5094,
5883,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 1080x720
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
55417,
144584,
258444,
463186,
1221,
761,
448,
8473,
2263,
9244,
8532,
13473,
15506,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 1920x1080
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
149746,
390554,
698314,
1251068,
3399,
2064,
1367,
22701,
6222,
24733,
22680,
36451,
41962,


------------------ Unit testing END--------------- 
****************************************
Trial Number 5
****************************************

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 240x160
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
2592,
6639,
11695,
21198,
56,
36,
20,
418,
114,
437,
415,
606,
763,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 320x240
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
5176,
13400,
23915,
42984,
118,
68,
40,
824,
232,
880,
825,
1259,
1431,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 480x320
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
10397,
26924,
48788,
87076,
252,
146,
82,
1731,
430,
1735,
1670,
2502,
2890,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 640x480
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
20977,
54735,
98146,
175739,
501,
292,
177,
3346,
864,
3521,
3342,
5095,
5847,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 1080x720
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
55449,
144550,
258425,
463179,
1217,
759,
455,
8476,
2264,
9236,
8534,
13469,
15537,


------------------ Unit testing END--------------- 

 ----------Saliency Detection Application Start -------
---------------Configuration details------------
Image resolution: 1920x1080
Platform: Native

**************STARTING UNIT TESTING OF MODULES************
Module  under test      = Time in usec
149936,
390401,
698623,
1251428,
3399,
2073,
1362,
22705,
6216,
24738,
22677,
36468,
41847,


------------------ Unit testing END--------------- 
