#ifndef FILTER2D_H
#define FILTER2D_H
#include <stdint.h>
// general 2D filter which uses 3x3 kernel. Built for ARM
void s_filter2D(uint8_t *image, int *kernel, uint8_t *dest_image, int max_rows, int max_cols);

#endif // FILTER2D_H
