

#ifndef SALIENCY_API_H
#define SALIENCY_API_H
#include <stdint.h>

typedef struct
{
    uint8_t *img_data;
    int no_rows;
    int no_cols;
} Mat;

// 2D filter types
typedef enum  
{
	GAUSSIAN = 0,
	GABOR_0,
	GABOR_45,
	GABOR_90,
	GABOR_135,
	BILINEAR
	// ADD MORE HERE
} FILTER_TYPE;

// API to add two images. This does saturate add and devide by 2. Same as averaging two images
void img_add(Mat *img_1, Mat *img_2, Mat *img_out);

// API to normalize the image
void img_normalize(Mat *in_img, Mat *norm_img);

// API to subsample image by factor of 2
void img_subsample(Mat *in_img, Mat *out_img);

// API to double the size of the image
void img_interpolate(Mat *in_img, Mat *out_img);

// API to subtract two images. This finds absolute difference between 2 images
void img_subtract(Mat *img_1, Mat *img_2, Mat *diff_img);

// API to do Gaussian filter on imput image using 3x3 Gaussian kernel
void gaussian_blur(Mat *in_img, Mat *out_img);

// API to do 2D filtering on image using 3x3 kernel of specified type
void filter2D(Mat *in_img, Mat *out_img, FILTER_TYPE filter_type);

#endif // SALIENCY_API_H
