#include <stdio.h>
#include <math.h>
#include "img_resize.h"
#include "filter2D.h"
// Downsample by 2

 int lin_int_kernel[9] = {1,2,1,2,4,2,1,2,1};


void s_img_subsample(uint8_t *in_img, uint8_t *out_img, int max_rows, int max_cols)
{
	int m = ceil(max_rows/2);
	int n = ceil(max_cols/2);
	int row, col;
	
	uint8_t *sub_img = (uint8_t *) malloc(m*n*sizeof(uint8_t));
	// drop alternate pixels
	for (row = 0; row < m; row++)
	{
		for (col = 0; col < n; col++)
		{
			*(sub_img + n*row + col) = *(in_img + 2*max_cols*row + 2*col);
		}
	}
	// Now apply filter to remove blocking effects
	s_filter2D(sub_img, lin_int_kernel, out_img, m, n);
	free(sub_img);
}

// Interpolate by factor of 2
void s_img_interpolate(uint8_t *in_img, uint8_t * out_img, int max_rows, int max_cols) 
{ 
	int row, col;
        int new_max_rows = 2*max_rows;
        int new_max_cols = 2*max_cols;
        uint8_t *int_img = (uint8_t *) malloc(new_max_rows*new_max_cols*sizeof(uint8_t));
        // polulate original pixels in the output image
        for (row = 0; row < max_rows; row++)
        {
                for(col = 0; col < max_cols; col++)
                {
                        *(int_img + new_max_cols*2*row + 2*col) = *(in_img + max_cols*row + col);
                }
        }

        // now use filter2D API to do the interpolation
        s_filter2D(int_img, lin_int_kernel, out_img, new_max_rows, new_max_cols);
        free(int_img);

}


