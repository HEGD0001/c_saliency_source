#!/bin/sh
rm log.txt
make clean
make
for trial in 1 2 3 4 5
do
echo "****************************************" >> log.txt
echo "Trial Number"  $trial >> log.txt
echo "****************************************" >> log.txt
./saliency_app 160 240 >> log.txt
./saliency_app 240 320 >> log.txt
./saliency_app 320 480 >> log.txt
./saliency_app 480 640 >> log.txt
./saliency_app 720 1080 >> log.txt
./saliency_app 1080 1920 >> log.txt
done
