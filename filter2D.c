/*
 * Author: Gopalakrishna Hegde
 * Date: 9 Nov 2014
 * filename: filter2D.c
 *
*/


#include <stdio.h>
#include "filter2D.h"

// 2D filter API for ARM
void s_filter2D(uint8_t *image, int *kernel, uint8_t *dest_image, int M, int N)
{
	int row, col, kernel_row, kernel_col;
	uint16_t sop;
	int scale_fix;
	scale_fix = 0;
	// Find scaling factor 
        for(row = 0; row < 9; row++)
        {
                scale_fix += *(kernel + row);
        }
        scale_fix = (int)(256 /scale_fix);              // convert floating point to fixed point. We use 8 bits to represent this scaling factor.

	for (row = 0; row < M-2; row++)
	{
		for (col = 0; col < N-2; col++)
		{
			sop = 0;
			for ( kernel_row = 0; kernel_row < 3; kernel_row ++)
			{
				for (kernel_col = 0; kernel_col < 3; kernel_col++)
				{
					sop += (*(kernel + 3*kernel_row + kernel_col)) * (*(image + N*row + col + N*kernel_row + kernel_col));
				}
			}
			sop = (sop * scale_fix)>> 8;
			
			*(dest_image + N*row + col) = sop;
		}
	}
}


