#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/time.h>
#include "saliency_api.h"

#ifdef DEBUG
#define LOG(ARGS...) printf(ARGS)
#else
#define LOG(ARGS...) 
#endif


#define PYR_DEPTH 4 		// including original image


static void intensity_image(Mat *red_ch, Mat *green_ch, Mat *blue_ch, Mat*int_img);
static int intensity_map(Mat *int_img, Mat *int_map);
static int get_color_map(Mat * red, Mat *green, Mat *rg_map);
static int orientation_map(Mat *int_img, Mat *orient_map);
static void pyramid_filter(Mat *in_img, Mat *img_pyr);
static void center_surround(Mat *surround, Mat *center, Mat *cs_out, int ratio);
int find_saliency_map(void);


long get_timestamp();
void print_time(long start_time, long stop_time);

// returns current time in milliseconds
long get_timestamp()
{
	
	struct timeval current_time;
    gettimeofday(&current_time, NULL);
    return (current_time.tv_sec * 1000000 + current_time.tv_usec);
}

void print_time(long start_time, long stop_time)
{
	printf("%ld,\n", stop_time-start_time);
}

int MAX_ROWS = 256, MAX_COLS = 256;
int main(int argc, char **argv)
{
	

// Setup test images
	Mat test_img1, test_img2, test_img3, test_img4, res_img;
	int level;
	Mat pyramid[PYR_DEPTH+1];
	long start_time, stop_time;


	MAX_ROWS = atoi(argv[1]);
	MAX_COLS = atoi(argv[2]);
	test_img1.img_data = (uint8_t*)malloc(MAX_ROWS*MAX_COLS*sizeof(uint8_t));
	test_img2.img_data = (uint8_t*)malloc(MAX_ROWS*MAX_COLS*sizeof(uint8_t));
	test_img3.img_data = (uint8_t*)malloc((MAX_ROWS*MAX_COLS)/4);
	test_img4.img_data = (uint8_t*)malloc((MAX_ROWS*MAX_COLS)/16);
	res_img.img_data = (uint8_t*)malloc(MAX_ROWS*MAX_COLS*sizeof(uint8_t));
	test_img1.no_rows = MAX_ROWS;
	test_img1.no_cols = MAX_COLS;
	test_img2.no_rows = MAX_ROWS;
	test_img2.no_cols = MAX_COLS;
	test_img3.no_rows = MAX_ROWS/2;
	test_img3.no_cols = MAX_COLS/2;
	test_img4.no_rows = MAX_ROWS/4;
	test_img4.no_cols = MAX_COLS/4;
	memset(test_img1.img_data, 200, test_img1.no_rows*test_img1.no_cols);
	memset(test_img2.img_data, 28,  test_img2.no_rows*test_img2.no_cols);
	memset(test_img3.img_data, 165, test_img3.no_rows*test_img3.no_cols);
	memset(test_img4.img_data, 120, test_img4.no_rows*test_img4.no_cols);
	// allocate memory for intensity pyramid
        for (level = 0; level <= PYR_DEPTH; level++)
        {
            if (NULL == (pyramid[level].img_data = (uint8_t *)malloc(MAX_ROWS * MAX_COLS)))
            {
                printf(" Memory allocation failed..\n");
                return -1;
            }
        }

	printf("\n ----------Saliency Detection Application Start -------\n");
	printf("---------------Configuration details------------\n");
	printf("Image resolution: %dx%d\n", MAX_COLS, MAX_ROWS);
#ifdef MXP
	printf("Platform : MXP\n");
#ifdef ROW_BUFFERING
	printf("Row buffering: ENABLED\n");
#else
	printf("Row buffering: DISABLED\n");
#endif
	printf("Double buffering: ENABLED\n");

#else
	printf("Platform: Native\n");
#endif
	printf("\n**************STARTING UNIT TESTING OF MODULES************\n");
	printf("Module  under test      = Time in usec");

	printf("\n");
//------------------------------------------------------
	start_time = get_timestamp();
	find_saliency_map();
    
	stop_time = get_timestamp();
//    printf("\nSaliency map                     = ");
	print_time(start_time, stop_time);
//------------------------------------------------------
// Image add test
	start_time = get_timestamp();
	img_add(&test_img1, &test_img2, &res_img);

	stop_time = get_timestamp();
//    printf("\nImage add                        = ");
	print_time(start_time, stop_time);


//------------------------------------------------------

//------------------------------------------------------
// Image subtraction test
	start_time = get_timestamp();
    img_subtract(&test_img1, &test_img2, &res_img);

	stop_time = get_timestamp();
 //   printf("\nImage sub                        = ");
	print_time(start_time, stop_time);


//------------------------------------------------------

//------------------------------------------------------
// Image normalization test
	start_time = get_timestamp();
    img_normalize(&test_img1, &test_img2);

	stop_time = get_timestamp();
   // printf("\nImage normalize                  = ");
	print_time(start_time, stop_time);


//------------------------------------------------------

//------------------------------------------------------
// Image filtering test
	start_time = get_timestamp();
    filter2D(&test_img1, &test_img2, GAUSSIAN);

	stop_time = get_timestamp();
   // printf("\nImage filtering                  = ");
	print_time(start_time, stop_time);


//------------------------------------------------------

//------------------------------------------------------
// Image subsample test
	start_time = get_timestamp();
    img_subsample(&test_img1, &test_img3);

	stop_time = get_timestamp();
    //printf("\nImage subsampling by 2           = ");
	print_time(start_time, stop_time);


//------------------------------------------------------

//------------------------------------------------------
// Image interpolation test
	start_time = get_timestamp();
    img_interpolate(&test_img3, &test_img1);

	stop_time = get_timestamp();
    //printf("\nImage interpolation by 2         = ");
	print_time(start_time, stop_time);


//------------------------------------------------------

//------------------------------------------------------
// Image gaussian blur test
	start_time = get_timestamp();
    gaussian_blur(&test_img2, &test_img1);

	stop_time = get_timestamp();
    //printf("\nGaussian blur                    = ");
	print_time(start_time, stop_time);


//------------------------------------------------------

//------------------------------------------------------
// Center-surround test
	start_time = get_timestamp();
    center_surround(&test_img2, &test_img4, &test_img1, 2);

	stop_time = get_timestamp();
    //printf("\nCenter-surround                  = ");
	print_time(start_time, stop_time);


//------------------------------------------------------

//------------------------------------------------------
// Pyramid filter test
	start_time = get_timestamp();
    pyramid_filter(&test_img1, pyramid);

	stop_time = get_timestamp();
    //printf("\nPyramid filter                   = ");
	print_time(start_time, stop_time);


//------------------------------------------------------

    printf("\n\n------------------ Unit testing END--------------- \n");

    // de-allocate memory of  pyramid
    for (level = 0; level <= PYR_DEPTH; level++)
    {
        free(pyramid[level].img_data);
    }

	free(test_img1.img_data);
	free(test_img2.img_data);
	free(test_img3.img_data);
	free(test_img4.img_data);
	free(res_img.img_data);
	return 0;
}

int find_saliency_map(void)
{
	Mat red_ch, green_ch, blue_ch, yellow_ch;
	Mat int_map, rg_map, by_map, ori_map;			// all these maps are at level 1 ie. size = MAX_ROWS/2, MAX_COLS/2
	Mat int_img, norm_int_map, norm_color_map, norm_ori_map,saliency_map;
	long start_time, stop_time;

	// Memory allocation
	if(	(NULL == (red_ch.img_data = (uint8_t *)malloc(MAX_ROWS*MAX_COLS))) ||
		(NULL == (green_ch.img_data = (uint8_t *)malloc(MAX_ROWS*MAX_COLS))) ||
		(NULL == (blue_ch.img_data = (uint8_t *)malloc(MAX_ROWS*MAX_COLS))) ||
		(NULL == (yellow_ch.img_data = (uint8_t *)malloc(MAX_ROWS*MAX_COLS))) ||
		(NULL == (int_img.img_data = (uint8_t *)malloc(MAX_ROWS*MAX_COLS))) ||
		(NULL == (int_map.img_data = (uint8_t *)malloc((MAX_ROWS*MAX_COLS)/4))) ||
		(NULL == (ori_map.img_data = (uint8_t *)malloc((MAX_ROWS*MAX_COLS)/4))) ||
		(NULL == (rg_map.img_data = (uint8_t *)malloc((MAX_ROWS*MAX_COLS)/4))) ||
		(NULL == (by_map.img_data = (uint8_t *)malloc((MAX_ROWS*MAX_COLS)/4))) ||
		(NULL == (norm_int_map.img_data = (uint8_t *)malloc((MAX_ROWS*MAX_COLS)/4))) ||
		(NULL == (norm_color_map.img_data = (uint8_t *)malloc((MAX_ROWS*MAX_COLS)/4))) ||
		(NULL == (norm_ori_map.img_data = (uint8_t *)malloc((MAX_ROWS*MAX_COLS)/4))) ||
		(NULL == (saliency_map.img_data = (uint8_t *)malloc((MAX_ROWS*MAX_COLS)/4)))
		)
	{
		printf(" malloc failed.. exiting..!\n");
		return -1;
	}
	// Init image dimensions
	red_ch.no_rows = MAX_ROWS;
	red_ch.no_cols = MAX_COLS;
	green_ch.no_rows = MAX_ROWS;
   	green_ch.no_cols = MAX_COLS;
	blue_ch.no_rows = MAX_ROWS;
   	blue_ch.no_cols = MAX_COLS;
	yellow_ch.no_rows = MAX_ROWS;
   	yellow_ch.no_cols = MAX_COLS;

	// find intensity image
	intensity_image(&red_ch, &green_ch, &blue_ch, &int_img);

	
	start_time = get_timestamp();
	// find intensity map
	if (0 !=  intensity_map(&int_img, &int_map))
	{
		printf("main: intensity map generation failed\n");
		return -1;
	}
	
	stop_time = get_timestamp();
   // printf("\nIntensity map                    = ");
	print_time(start_time, stop_time);
	LOG("%s Generated intensity map \n", __func__);


	start_time = get_timestamp();
	// find color maps
	if (0 != get_color_map(&red_ch, &green_ch, &rg_map))
    {
        printf("main: RG map generation failed\n");
        return -1;
    }


	if (0 != get_color_map(&blue_ch, &yellow_ch, &by_map))
    {
        printf("main: BY  map generation failed\n");
        return -1;
    }
	stop_time = get_timestamp();
    //printf("\nColor map                        = ");
	print_time(start_time, stop_time);

    LOG("%s Generated color map \n", __func__);


	start_time = get_timestamp();
    if (0 != orientation_map(&int_img, &ori_map))
    {
        printf("%s failed to generate orientation map\n", __func__);
        return -1;
    }

	stop_time = get_timestamp();
    //printf("\nOrientation map                  = ");
	print_time(start_time, stop_time);

	// average of rg and by map and store it in rg_map
	img_add(&rg_map, &by_map, &rg_map);

	// normalization of maps
	img_normalize(&int_map, &norm_int_map);
	img_normalize(&rg_map, &norm_color_map);
	img_normalize(&ori_map, &norm_ori_map);
	// average both maps to get final saliency map
	img_add(&norm_int_map, &norm_color_map, &saliency_map);

	img_add(&saliency_map, &norm_ori_map, &saliency_map);

	// memory de-allocation
	free(red_ch.img_data);	
	free(green_ch.img_data);	
	free(blue_ch.img_data);	
	free(yellow_ch.img_data);	
	free(int_img.img_data);	
	free(int_map.img_data);
	free(ori_map.img_data);
	free(rg_map.img_data);	
	free(by_map.img_data);	
	free(norm_int_map.img_data);
	free(norm_color_map.img_data);
	free(norm_ori_map.img_data);
	free(saliency_map.img_data);	
	return 0;
}

static void intensity_image(Mat *red_ch, Mat *green_ch, Mat *blue_ch, Mat*int_img)
{
	Mat temp_sum;

	LOG("%s : Entry\n", __func__);

	temp_sum.img_data = (uint8_t *) malloc(red_ch->no_rows*red_ch->no_cols);
	temp_sum.no_rows = red_ch->no_rows;
	temp_sum.no_cols = red_ch->no_cols;

	img_add(red_ch, green_ch, &temp_sum);	
	img_add(&temp_sum, blue_ch, int_img);

	free(temp_sum.img_data);

	LOG("%s: Exit\n", __func__);
}

static int intensity_map(Mat *int_img, Mat *int_map)
{
		Mat int_pyramid[PYR_DEPTH+1];
		Mat map_02, map_03, map_13, map_14, map_02_03, map_13_14, map_1;
		int level;

		LOG("%s : Entry\n", __func__);

		// allocate memory for intensity pyramid
		for (level = 0; level <= PYR_DEPTH; level++)
		{
			if (NULL == (int_pyramid[level].img_data = (uint8_t *)malloc(int_img->no_rows * int_img->no_cols)))
			{
				printf("intensity_map: Memory allocation failed..\n");				
				return -1;
			}
		}
		
		// allocate memory for center-surround differences
		if ((NULL == (map_02.img_data = (uint8_t *)malloc(int_img->no_rows * int_img->no_cols))) ||
			(NULL == (map_03.img_data = (uint8_t *)malloc(int_img->no_rows * int_img->no_cols))) ||
			(NULL == (map_13.img_data = (uint8_t *)malloc((int_img->no_rows * int_img->no_cols)/4))) ||
			(NULL == (map_14.img_data = (uint8_t *)malloc((int_img->no_rows * int_img->no_cols)/4))) ||
			(NULL == (map_02_03.img_data = (uint8_t *)malloc(int_img->no_rows * int_img->no_cols))) ||
			(NULL == (map_13_14.img_data = (uint8_t *)malloc((int_img->no_rows * int_img->no_cols)/4))) ||
			(NULL == (map_1.img_data = (uint8_t *)malloc((int_img->no_rows * int_img->no_cols)/4)))
			)
		{
                	printf("intensity_map: Memory allocation failed..\n");
                	return -1;
			
		}

		// build intensity pyramid
		pyramid_filter(int_img, int_pyramid);

		LOG("intensity_map: built the intensity pyramid\n");	

		center_surround(&int_pyramid[0], &int_pyramid[2], &map_02, 2);
		center_surround(&int_pyramid[0], &int_pyramid[3], &map_03, 3);
		// add two maps and bring to level 1
		img_add(&map_02, &map_03, &map_02_03);
		img_subsample(&map_02_03, &map_1);
		
		center_surround(&int_pyramid[1], &int_pyramid[3], &map_13, 2);
		center_surround(&int_pyramid[1], &int_pyramid[4], &map_14, 3);
		img_add(&map_13, &map_14, &map_13_14);

		// final intensity map
		img_add(&map_1, &map_13_14, int_map);




        // de-allocate memory for intensity pyramid
        for (level = 0; level <= PYR_DEPTH; level++)
        {
            free(int_pyramid[level].img_data);
        }
		free(map_02.img_data);
		free(map_03.img_data);
		free(map_13.img_data);
		free(map_14.img_data);
		free(map_02_03.img_data);
		free(map_13_14.img_data);
		free(map_1.img_data);
		LOG("%s : Exit\n", __func__);
		return 0;
}

// find color map. The same API can be used to find BY and RG map
static int get_color_map(Mat * red, Mat *green, Mat *rg_map)
{
	int	level;
	Mat red_pyr[PYR_DEPTH+1], green_pyr[PYR_DEPTH+1];
	Mat map_02, map_03, map_13, map_14, map_02_03, map_13_14, map_1;
	// Allocate memory
	for (level = 0; level <= PYR_DEPTH; level++)
    {
    	if ((NULL == (red_pyr[level].img_data = (uint8_t *)malloc(red->no_rows * red->no_cols))) ||
			(NULL == (green_pyr[level].img_data = (uint8_t *)malloc(green->no_rows * green->no_cols)))
			)
        {
			printf("get_color_map: memory allocation failed!!\n");
            return -1;
        }
	}

        // allocate memory for center-surround differences
	if ((NULL == (map_02.img_data = (uint8_t *)malloc(red->no_rows * red->no_cols))) ||
        (NULL == (map_03.img_data = (uint8_t *)malloc(red->no_rows * red->no_cols))) ||
        (NULL == (map_13.img_data = (uint8_t *)malloc((red->no_rows * red->no_cols)/4))) ||
        (NULL == (map_14.img_data = (uint8_t *)malloc((red->no_rows * red->no_cols)/4))) ||
        (NULL == (map_02_03.img_data = (uint8_t *)malloc(red->no_rows * red->no_cols))) ||
        (NULL == (map_13_14.img_data = (uint8_t *)malloc((red->no_rows * red->no_cols)/4))) ||
        (NULL == (map_1.img_data = (uint8_t *)malloc((red->no_rows * red->no_cols)/4)))
       )
    {
        printf("intensity_map: Memory allocation failed..\n");
    	return -1;

    }


	// find red and green pyramid
	pyramid_filter(red, red_pyr);
	pyramid_filter(green, green_pyr);

	// red - green differences. We use red_pyr to store these differences instead of allocating different memory
	img_subtract(&red_pyr[0], &green_pyr[0], &red_pyr[0]);
	img_subtract(&red_pyr[1], &green_pyr[1], &red_pyr[1]);
	
	img_subtract(&green_pyr[2], &red_pyr[2],&red_pyr[2]);
	img_subtract(&green_pyr[3], &red_pyr[3],&red_pyr[3]);
	img_subtract(&green_pyr[4], &red_pyr[4],&red_pyr[4]);

	center_surround(&red_pyr[0], &red_pyr[2], &map_02, 2);
    center_surround(&red_pyr[0], &red_pyr[3], &map_03, 3);
        // add two maps and bring to level 1
    img_add(&map_02, &map_03, &map_02_03);
    img_subsample(&map_02_03, &map_1);

    center_surround(&red_pyr[1], &red_pyr[3], &map_13, 2);
    center_surround(&red_pyr[1], &red_pyr[4], &map_14, 3);
    img_add(&map_13, &map_14, &map_13_14);

    // final intensity map
    img_add(&map_1, &map_13_14, rg_map);


	// de-allocate memory
	for (level = 0; level <= PYR_DEPTH; level++)
    {
		free(red_pyr[level].img_data);
		free(green_pyr[level].img_data);
    }
	
	free(map_02.img_data);
    free(map_03.img_data);
    free(map_13.img_data);
    free(map_14.img_data);
    free(map_02_03.img_data);
    free(map_13_14.img_data);
    free(map_1.img_data);
	
	return 0;

}


static int orientation_map(Mat *int_img, Mat *orient_map)
{
    Mat gabor_img[4];
    Mat ori_map[4];
    Mat temp1, temp2;
    int deg;

    // memory allocation
    for (deg = 0; deg < 4; deg++)
    {
        if( (NULL == (gabor_img[deg].img_data = (uint8_t *)malloc(int_img->no_rows * int_img->no_cols))) ||
            (NULL == (ori_map[deg].img_data = (uint8_t *)malloc(int_img->no_rows/2 * int_img->no_cols/2)))
        )
        {
            printf("orientation_map: Memory allocation failed..\n");
            return -1;
        }
    }
    if ( (NULL == (temp1.img_data = (uint8_t*) malloc(int_img->no_rows/2*int_img->no_cols/2))) ||
        (NULL == (temp2.img_data = (uint8_t*) malloc(int_img->no_rows/2*int_img->no_cols/2)))
    )
    {

        printf("orientation_map: Memory allocation failed..\n");
        return -1;
    }
    // find gabor images
    filter2D(int_img, &gabor_img[0], GABOR_0);
    filter2D(int_img, &gabor_img[1], GABOR_45);
    filter2D(int_img, &gabor_img[2], GABOR_90);
    filter2D(int_img, &gabor_img[3], GABOR_135);

    // Find intensity maps of Gabor images
    intensity_map(&gabor_img[0], &ori_map[0]);
    intensity_map(&gabor_img[1], &ori_map[1]);
    intensity_map(&gabor_img[2], &ori_map[2]);
    intensity_map(&gabor_img[3], &ori_map[3]);
    // combine maps
    img_add(&ori_map[0], &ori_map[1], &temp1);
    img_add(&ori_map[2], &ori_map[3], &temp2);

    // final orientation map
    img_add(&temp1, &temp2, orient_map);

    // deallocate memory
    for (deg = 0; deg < 4; deg++)
    {
        free(gabor_img[deg].img_data);
        free(ori_map[deg].img_data);
    }
    free(temp1.img_data);
    free(temp2.img_data);
    return 0;

}


// second argument is pointer to array of struct. caller should allocate memory and pass this pointer
static void pyramid_filter(Mat *in_img, Mat *img_pyr)
{
	int level;
	Mat filtered_img, subsampled_img;

	LOG("%s: Entry\n", __func__);

	filtered_img.img_data = (uint8_t *) malloc(in_img->no_rows*in_img->no_cols*sizeof(uint8_t));
	subsampled_img.img_data = (uint8_t *) malloc((in_img->no_rows*in_img->no_cols*sizeof(uint8_t))/4);

	// level 0 of the pyramid is image itself
	memcpy(img_pyr[0].img_data, in_img->img_data, in_img->no_rows*in_img->no_cols*sizeof(uint8_t));
	img_pyr[0].no_rows = in_img->no_rows;
	img_pyr[0].no_cols = in_img->no_cols;
	
	for(level = 0; level < PYR_DEPTH; level++)
	{
		gaussian_blur(&img_pyr[level], &filtered_img); // this api shall initialize size of filtered_img
		img_subsample(&filtered_img, &img_pyr[level+1]);	// size of subsampled img size shall be set by this API		
	}
	
	free(filtered_img.img_data);
	free(subsampled_img.img_data);

	LOG("%s: Exit\n", __func__);
	return ;
}

// TODO: use uncached malloc for MXP. better to use MACRO based compiler switch
static void center_surround(Mat *surround, Mat *center, Mat *cs_out, int ratio)
{
	int  i;
	Mat center_rescaled, temp_img;

	LOG("%s: Entry\n", __func__);

	center_rescaled.img_data = (uint8_t *)malloc(surround->no_rows*surround->no_cols);

	temp_img.img_data = (uint8_t *)malloc(surround->no_rows*surround->no_cols);
    temp_img.no_rows = center->no_rows;
    temp_img.no_cols = center->no_cols;
	memcpy(temp_img.img_data, center->img_data, center->no_rows*center->no_cols);

	for (i = 0; i < ratio; i++)
	{
		img_interpolate(&temp_img, &center_rescaled);
		temp_img.no_rows = center_rescaled.no_rows;
   		temp_img.no_cols = center_rescaled.no_cols;
   		memcpy(temp_img.img_data, center_rescaled.img_data, center_rescaled.no_rows*center_rescaled.no_cols);
	}

	img_subtract(&center_rescaled, surround, cs_out);
	free(temp_img.img_data);
	free(center_rescaled.img_data);

	LOG("%s: Exit\n", __func__);
}
