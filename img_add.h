
#ifndef IMG_ADD_H
#define IMG_ADD_H
#include <stdint.h>

// API to add 2 images using ARM
void s_img_add_weighted(uint8_t *image1, uint8_t *image2, uint8_t *res_image, int M, int N, int scale);

#endif // IMG_ADD_H
