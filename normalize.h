

#ifndef NORMALIZE_H
#define NORMALIZE_H
#include <stdint.h>

void s_img_normalize(uint8_t *image1, uint8_t *norm_img,  int M, int N);

uint8_t max_pixel(uint8_t *image, int M, int N);
#endif
