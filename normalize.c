#include "normalize.h"
#include <stdio.h>
#define NUM_BIT_SHIFT 7


void s_img_normalize(uint8_t *image1, uint8_t *norm_img,  int M, int N)
{
    int row, col;
    uint16_t temp;
	uint8_t max;
	max = max_pixel(image1, M, N);
	if (max <= 0)
	{
		printf("Max pixel is 0\n");
		return;
	}
        uint8_t scale = UINT8_MAX *128/max;
        for (row = 0; row < M; row++)
        {
                for(col = 0; col < N; col++)
                {
                        temp  = *(image1+row*N+col) * scale;
			*(norm_img + N*row + col) = temp >> 7;
                }
        }

}

uint8_t max_pixel(uint8_t *image, int M, int N)
{
        int row, col;
        uint8_t max = 0;
        for (row = 0; row < M; row++)
        {
                for (col = 0; col < N; col++)
                {
                        if (*(image+N*row+col) > max)
                        {
                                max = *(image+N*row+col);
                        }
                }
        }
	return max;
}
