
#ifndef IMG_SUB_H
#define IMG_SUB_H
#include <stdint.h>

// API to subtract 2 images using ARM
void s_img_sub(uint8_t *image1, uint8_t *image2, uint8_t *res_image, int M, int N);

#endif // IMG_SUB_H
