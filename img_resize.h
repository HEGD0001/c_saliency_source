#ifndef IMG_RESIZE_H
#define IMG_RESIZE_H
#include <stdint.h>
#include <stdlib.h>


// Interpolate by factor of 2 using ARM
void s_img_interpolate(uint8_t *in_img, uint8_t *out_img, int max_rows, int max_cols);

// Downsample by 2 using ARM
void s_img_subsample(uint8_t *in_img, uint8_t *out_img, int max_rows, int max_cols);



#endif
